﻿using dealpos_library.HttpClientExtension;
using dealpos_library.Sematext;
using dealpos_library.Sematext.LogSchema;
using dealpos_library.Sematext.LogSchema.WebHook;
using marketplace_library.Model;
using marketplace_library.Model.config;
using System.Text.Json;

namespace WebApp.BLL
{
    public class SematextTaskServices
    {
        private const string endPoint = "https://endz7l6mygeai.x.pipedream.net";
        private IServiceScopeFactory _scopeFactory;
        private HttpClient _httpClient;
        public SematextTaskServices(IServiceScopeFactory scopeFactory, HttpClient client)
        {
            _scopeFactory = scopeFactory;
            _httpClient = client;
        }

        public void Run(object req)
        {
            Task.Run(() =>
            {
                for (var i = 0; i <= 100000; i++)
                {
                    using (var scope = _scopeFactory.CreateScope())
                    {
                        IHttpClientFactory httpClientFactory = scope.ServiceProvider.GetRequiredService<IHttpClientFactory>();
                        WriteLog(req, httpClientFactory);
                    }
                }
            });

            
            _httpClient.PostJSON(endPoint, req);
        }

        private void WriteLog(dynamic req, IHttpClientFactory clientFactory)
        {
            SematextConfig config = MarketplaceAppConfig.Instance.Sematext;
            SematextWebHookLog sm = new SematextWebHookLog();
            sm.host = "localhost";
            sm.logType = "Test Queue";
            sm.source = "Local tafta";
            sm.message = "SNAT ports exhaustion";

            sm.log = new SematextWebHookLogData();
            sm.log.request = JsonSerializer.Serialize(req);
            SematextHttpClient.Write(clientFactory, sm, config.TokenRaw);
        }
    }
}
