﻿using dealpos_library;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApp.BLL;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    public class QueueController : ControllerBase
    {
        private IHttpClientFactory _clientFactory;
        private IServiceScopeFactory _scopeFactory;
        public QueueController(IHttpClientFactory clientFactory, IServiceScopeFactory scopeFactory)
        {
            _clientFactory = clientFactory;
            _scopeFactory = scopeFactory;
        }

        [HttpPost]
        public ActionResult Post([FromBody] JsonElement request)
        {
            object obj = PropertyChecker.ToObject<object>(request);
            SematextTaskServices s = new SematextTaskServices(_scopeFactory, _clientFactory.CreateClient());
            s.Run(obj);
            return Ok(obj);
        }
    }
}
