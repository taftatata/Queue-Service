using dealpos_library;
using marketplace_library.Model;

var builder = WebApplication.CreateBuilder(args);

ConfigurationManager configuration = builder.Configuration;

var appConfig = configuration.Get<MarketplaceAppConfig>();
builder.Services.AddSingleton<MarketplaceAppConfig>(appConfig);

// Add services to the container.

builder.Services.AddControllers();

/// <summary>
/// HttpClient
/// </summary>
builder.Services.AddHttpClient();

//Exclusive connection to Sematext
builder.Services.AddHttpClient(NamedHttpClients.Sematext, httpClient =>
{
    httpClient.BaseAddress = new Uri("https://logsene-receiver.sematext.com/");
}).ConfigureHttpMessageHandlerBuilder((c) =>
    new HttpClientHandler
    {
        MaxConnectionsPerServer = 10,
    }
);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
